#!/usr/bin/env bash
mkdir -p ~/nm/web/node_modules
ln -sn ~/nm/web/node_modules /vagrant/web/node_modules

cd /vagrant/web && npm install && bower --quiet --config.interactive=false --config.analytics=false install