exports.config = {

  seleniumAddress: 'http://localhost:4444/wd/hub',

  // http://jasmine.github.io/2.1/upgrading.html
  framework: 'jasmine2',

  // To run on multiple browsers online... using a local tunnel, very cool or the app could be deployed on QA!
  // http://stackoverflow.com/questions/25537919/running-protractor-tests-on-browserstack-automate
  // SAUCELABS: https://angular.github.io/protractor/#/server-setup
  capabilities: {
    'browserName': 'chrome'
  },

  specs: ['tests/integration/**/*.js'],

  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000
  }
};
