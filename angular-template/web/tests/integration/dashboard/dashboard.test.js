// http://product.moveline.com/testing-angular-apps-end-to-end-with-protractor.html
describe('by model', function() {
  it('should find an element by text', function() {
    browser.get("#/login");

    var helloWorld = element(by.binding('helloWorld'));

    expect(helloWorld.getText()).toEqual("Hello!");
  });
});
