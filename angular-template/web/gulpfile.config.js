var path = require('path');
var plugins = require('gulp-load-plugins')();

// Define paths
var config = {};
config.web = { path: {}, searchPath: {} };

// Define web configs
config.web.path.main        = path.join(__dirname);
config.web.path.tests       = path.join(__dirname, 'tests/');
config.web.path.app         = path.join(config.web.path.main, 'app/');
config.web.path.docs         = path.join(config.web.path.main, 'doc/');

config.web.path.dist = {};
config.web.path.dist.main   = path.join(config.web.path.main, 'dist/');
config.web.path.dist.web    = path.join(config.web.path.main, 'dist/app/');

config.web.searchPath.css   = path.join(config.web.path.app, 'css/**/*.css');
config.web.searchPath.scss  = [path.join(config.web.path.app, 'scss/**/*.scss')];
config.web.searchPath.js    = [
  path.join(config.web.path.app, 'modules/**/*.js'),
  path.join(config.web.path.app, 'components/**/*.js'),
  path.join(config.web.path.app, 'directives/**/*.js'),
  '!' + path.join(config.web.path.app + "modules/templates/templates.js")
];

config.web.searchPath.html  = path.join(config.web.path.app, '**/*.html');
config.web.searchPath.unitTests = path.join(config.web.path.main, 'tests/unit/**/*.js');
config.web.searchPath.integrationTests = path.join(config.web.path.main, 'tests/integration/**/*.js');
config.web.searchPath.templateCacheExclude = '!' + path.join(config.web.path.app,'modules/templateCache.js');

config.env = (plugins.util.env.env ? plugins.util.env.env : 'local');

module.exports = config;
