var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();
var path = require('path');
var config = require('./gulpfile.config.js');

var test = function () {

  gulp.task('test:integration', ['serve'], function(done) {
    gulp.src([config.web.searchPath.integrationTests])
      .pipe(plugins.angularProtractor({
        'configFile':  path.join(config.web.path.main, 'protractor.conf.js'),
        'args': ['--baseUrl', 'http://127.0.0.1:3000'],
        'autoStartStopServer': true,
        'debug': true
      }))
      .on('error', function (e) {
        throw e;
      });

    done();
  });

  gulp.task('test:web', ['templateCache'], function(done) {
    var KarmaServer = require('karma').Server;

    new KarmaServer({
      configFile: path.join(config.web.path.main, 'karma.conf.js'),
      singleRun: true
    }, done).start();

  });

  gulp.watch([config.web.searchPath.integrationTests], ['test:integration']);
  gulp.watch([config.web.searchPath.unitTests], ['test:web']);
}

module.exports = test();
