var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();
var path = require('path');
var config = require('./gulpfile.config.js');

var dist = function(webapp) {

  /********************************************************************/
  /* SETUP DISTRIBUTION TASKS *****************************************/
  /********************************************************************/
  gulp.task('dist:clean', function () {
    return gulp.src(path.join(config.web.path.dist.main,'**/*.*'), {read: false, force:true})
      .pipe(plugins.clean());
  });

  gulp.task('dist:copy', ['dist:clean','sass'], function() {
    return gulp.src([path.join(config.web.path.app,'**/*.*'), config.web.searchPath.templateCacheExclude])
      .pipe(gulp.dest(config.web.path.dist.web));
  });

  gulp.task('dist:copy_web_server', ['dist:copy'], function (){
    return gulp.src([path.join(config.web.path.main,'app.js'), path.join(config.web.path.main,'package.json')])
      .pipe(gulp.dest(config.web.path.dist.main));
  });

  gulp.task('dist:templateCache', ['dist:copy_web_server'], function() {

    return gulp.src(path.join(config.web.path.dist.web,'views/**/*.html'))
      .pipe(plugins.minifyHtml({ empty: true }))
      .pipe(plugins.angularTemplatecache({ root: 'views/'}))
      .pipe(gulp.dest(path.join(config.web.path.dist.web,'scripts/modules')));

  });

  gulp.task('dist:usemin',['dist:templateCache'],function() {
    return gulp.src([path.join(config.web.path.dist.web,'index.html')])
      .pipe(plugins.usemin({
        css: [ plugins.stripCssComments({ preserve: false }), plugins.minifyCss({ processImport: false }), plugins.rev() ],
        components: [ plugins.uglify(), 'concat', plugins.rev() ],
        js: [ plugins.ngAnnotate(), plugins.uglify(), 'concat', plugins.rev() ],
      }))
      .pipe(gulp.dest(config.web.path.dist.web));
  });

  gulp.task('dist:htmlmin', ['dist:usemin'], function() {

    return gulp.src(path.join(config.web.path.dist.web,'**/*.html'))
      .pipe(plugins.minifyHtml({ empty: true }))
      .pipe(gulp.dest(config.web.path.dist.web));

  });

  gulp.task('dist', ['dist:usemin', 'dist:htmlmin'], function () {

    plugins.util.log("Created distribution for environment [" + config.env + "]");

    return gulp.src([path.join(config.web.path.dist.web,'components'),
      path.join(config.web.path.dist.web,'lib'),
      path.join(config.web.path.dist.web,'css'),
      path.join(config.web.path.dist.web,'scss'),
      path.join(config.web.path.dist.web,'directives'),
      path.join(config.web.path.dist.web,'modules')], {read: false, force:true})
      .pipe(plugins.clean());
  });

  gulp.task('dist:serve',['dist'], function() {
    webapp = plugins.liveServer('app.js', { cwd: config.web.path.dist.main });
    webapp.start();
  });

}

module.exports = dist;
