//*****************************************************//
//*** Execute servers, compass & tests              ***//
//*****************************************************//
// We should not confuse with creating distribution
// for deployment. We can use any other tool or gulp task
// without complication the daily work process.
//
// We do not want a DEV distribution in which we constantly "copy" the working folder.
// But we do want a QA/PROD distribution with all correct procedures implemented.
//
// Although is possible to automate everything, I would initially:
//
// - Apply SASS/COMPASS compilation.
// - Apply SASS-LINT and JSHINT. (Complemented by IDEs like WebStorm)
//
//*****************************************************//
//
// Some help with sintax fix automatically

// https://github.com/kirjs/gulp-fixmyjs
// https://developers.google.com/closure/utilities/docs/linter_howto?csw=1
//
//*****************************************************//

var gulp = require('gulp');
var plugins = require('gulp-load-plugins')();
var path = require('path');
var config = require('./gulpfile.config.js');
var webapp = null;

require('./gulpfile.test.js');
require('./gulpfile.dist.js')(webapp);

gulp.task('doc', [], function () {
  return gulp.src(config.web.searchPath.js)
    .pipe(plugins.ngdocs.process())
    .pipe(gulp.dest(config.web.path.docs));
});

gulp.task('sass', function() {

  return gulp.src(config.web.searchPath.scss)
    .pipe(plugins.cached('sass'))
    .pipe(plugins.scssLint())
    .pipe(plugins.compass({
        project: config.web.path.app,
        sass: 'layout',
        image: 'public/images'
    }))
    .pipe(plugins.autoprefixer())
    .pipe(gulp.dest("./css"));

});

gulp.task('eslint', function() {

    return gulp
        .src(config.web.searchPath.js)
        .pipe(plugins.cached('eslint'))
        .pipe(plugins.eslint())
        .pipe(plugins.eslint.format());

});

gulp.task('templateCache', function(done) {

  var stream = gulp.src(config.web.searchPath.html)
    .pipe(plugins.minifyHtml({ empty: true }))
    .pipe(plugins.angularTemplatecache())
    .pipe(gulp.dest(path.join(config.web.path.app,'modules/templates/')));

  stream.on('end', function() {
    if (webapp) {
      webapp.notify.apply(webapp, [path.join(config.web.path.app,'modules/templates/templates.js')]);
    }
    done();
  });

});

gulp.task('serve', ['sass', 'eslint', 'templateCache'], function() {
  plugins.util.log("Starting server");
  webapp = plugins.liveServer('app.js', { cwd: config.web.path.main });
  webapp.start();

  //use gulp.watch to trigger server actions(notify, start or stop)
  gulp.watch([config.web.searchPath.css, config.web.searchPath.js, config.web.searchPath.html], function (file) {
    webapp.notify.apply(webapp, [file]);
  });

  gulp.watch(config.web.searchPath.scss   , ['sass']);
  gulp.watch(config.web.searchPath.html   , ['templateCache']);
  gulp.watch([config.web.searchPath.js, '!**/templates/templates.js']     , ['eslint']);
});

gulp.task('default', ['serve']);

var exit = function(code) {
  console.log('About to exit with code:', code);

  if (webapp){
    webapp.stop();
  }
};

process.on('sigterm', exit);
process.on('exit', exit);