(function() {
  'use strict';

  /**
   *
   * @ngdoc service
   * @name app.dashboard.dashboardService
   * @description
   *
   * Control the flow of the Dashboard page
   *
   * @requires $q
   * @requires $timeout
   *
   * @param {$q} $q       Injected dashboardService
   * @param {$q} $timeout Injected dashboardService
   *
   * @returns {this} An instance of DashboardService
   */
  function dashboardService($q, $timeout) {

    var find = function() {
      return $q(function(resolve) {

        $timeout(function() {

          resolve({
            welcomeMessage: 'Hello!'
          });

        }, 2000);

      });
    };

    var isUnique = function(key, property, currentValue) {

      return $q(function(resolve) {

        $timeout(function() {

          if (currentValue === 'admin@admin.com') {
            resolve(true);
          }
          else {
            resolve(false);
          }

        }, 2000);

      });
    };

    return {
      find: find,
      isUnique: isUnique
    };
  }

  dashboardService.$inject = ['$q', '$timeout'];

  angular.module('app.dashboard').service('dashboardService', dashboardService);
})();