(function() {
  'use strict';

  /**
   *
   * @ngdoc controller
   * @name app.dashboard.DashboardController
   * @description
   *
   * Control the flow of the Dashboard page
   *
   * @requires  app.dashboard.dashboardService
   * @param     {app.dashboard.dashboardService}  dashboardService  Injected dashboardService
   */
  function DashboardController(dashboardService) {
    var vm = this;

    /**
     * Setup the view
     * @param {Object}  data                  The dashboard summary
     * @param {string}  data.welcomeMessage   Message to display on welcome
     **/
    function setupView(data) {
      vm.helloWorld = data.welcomeMessage;
    }

    /**
     * Do initial tasks on the controller
     */
    function activate() {
      vm.customer = {
        id: 21,
        email: 'leo@leo.com'
      };

      dashboardService.find().then(setupView);
    }

    activate();
  }

  DashboardController.$inject = ['dashboardService'];

  angular.module('app.dashboard').controller('DashboardController', DashboardController);
})();