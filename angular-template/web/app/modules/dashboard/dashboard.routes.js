(function() {
  'use strict';

  angular.module('app.dashboard').config(RouteConfig);

  RouteConfig.$inject = ['$stateProvider'];

  function RouteConfig($stateProvider) {

    $stateProvider
      .state('dashboard', {
        url: '/dashboard',
        templateUrl: 'modules/dashboard/dashboard.html',
        controller: 'DashboardController',
        controllerAs: 'vm'
      });

  }

})();