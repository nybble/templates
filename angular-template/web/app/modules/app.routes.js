(function() {
  'use strict';

  angular.module('myApp').config(RouteConfig);

  RouteConfig.$inject = ['$urlRouterProvider'];

  function RouteConfig($urlRouterProvider) {
    $urlRouterProvider.otherwise('/dashboard');
  }

})();