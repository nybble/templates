(function() {
  'use strict';

  angular.module('app.directives').directive('nibQuantitySelector', QuantitySelector);

  function QuantitySelector() {

    var directive = {
      restrict: 'E',
      templateUrl: 'modules/directives/nib-quantity-selector/quantity-selector.html',
      controller: QuantitySelectorController,
      controllerAs: 'vm',
      bindToController: true,
      replace: true,
      scope: {
        quantity: '@',
        selectedQuantity: '&'
      }
    };

    return directive;
  }

  function QuantitySelectorController() {
    var vm = this;

    vm.onlyNumbers = /^[0-9]+$/;

    vm.submit = function () {
      vm.selectedQuantity({newQuantity: vm.quantity});
    };
  }

})();
