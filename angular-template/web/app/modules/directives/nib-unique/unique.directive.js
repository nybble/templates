(function() {
  'use strict';

  var injections = ['$q', 'dashboardService'];
  function UniqueDirective($q, dashboardService) {

    var fnUniqueLink = function(scope, element, attrs, ngModel) {

      ngModel.$asyncValidators.unique =
        function (modelValue, viewValue) {

          var deferred = $q.defer(),
            currentValue = modelValue || viewValue,
            key = attrs.nibUniqueKey,
            property = attrs.nibUniqueProperty;

          if (key && property)
          {
            dashboardService.isUnique(key, property, currentValue)
              .then(function (unique) {
                if (unique) {
                  deferred.resolve(); //It's unique
                }
                else {
                  deferred.reject();  //Add unique to $errors
                }
              });
          }
          else
          {
            deferred.resolve();
          }

          return deferred.promise;
        };
    };

    return {
      restrict: 'A',
      require: 'ngModel',
      link : fnUniqueLink
    };
  }

  UniqueDirective.$inject = injections;
  angular.module('app.directives').directive('nibUnique', UniqueDirective);
})();
