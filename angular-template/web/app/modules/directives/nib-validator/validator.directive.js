(function() {
  'use strict';

  var patterns = {
    number: /^[0-9]+$/
  };

  function Validator() {

    var link = function(scope, element, attrs, ngModelCtrl) {
      var validationType = attrs.nibValidator,
        regex = new RegExp(patterns[validationType]);

      // Data moving from model to the view
      ngModelCtrl.$formatters.unshift(function(value) {
        setErrorIfInvalid(value);
        return value;
      });

      // Data moving from view to the model
      // This is better because the scope does not get update
      // with invalid data
      ngModelCtrl.$parsers.unshift(function(value) {
        var isValid = setErrorIfInvalid(value);
        return isValid ? value : undefined;
      });

      function setErrorIfInvalid(value) {
        var isValid = regex.test(value);
        ngModelCtrl.$setValidity('format', isValid);
        return isValid;
      }

//      ngModel.$validators.format = function(modelValue, viewValue){
//        var value = modelValue || viewValue;
//        return regex.test(value);
//      };
    };

    return {
      restrict: 'A',
      require: 'ngModel',
      link : link
    };
  }

  angular.module('app.directives').directive('nibValidator', Validator);
})();
