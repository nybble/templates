(function() {
  'use strict';

  angular.module('app.directives').config(OverlayConfig);

  OverlayConfig.$inject = ['$httpProvider'];

  function OverlayConfig($httpProvider) {
    $httpProvider.interceptors.push('overlayHttpInterceptor');
  }

})();

