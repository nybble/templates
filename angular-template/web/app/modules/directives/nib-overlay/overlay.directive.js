(function() {
  'use strict';

  angular.module('app.directives').directive('nibUnique', OverlayDirective);

  OverlayDirective.$inject = ['$q', '$timeout', 'overlayHttpInterceptor'];

  function OverlayDirective($q, $timeout, overlayHttpInterceptor) {

    var link = function(scope, element, attrs) {

      var overlayContainer = null,
        timerPromise = null,
        queue = [];

      init();

      function init() {
        wireUpHttpInterceptor();
        overlayContainer = element[0].firstChild;
      }

      function showOverlay() {
        overlayContainer.style.display = 'block';
      }

      function hideOverlay() {
        if (timerPromise)
          $timeout.cancel(timerPromise);

        overlayContainer.style.display = 'none';
      }

      function wireUpHttpInterceptor() {
        overlayHttpInterceptor.request = function(config) {
          processRequest();
          return config || $q.when(response);

        };

        overlayHttpInterceptor.response = function(response) {
          processResponse();
          return config || $q.when(response);
        };

        overlayHttpInterceptor.responseError = function(rejection) {
          processResponse();
          return config || $q.reject(rejection);
        };
      }

      function processRequest() {
        queue.push({});
        if (queue.length === 1) {
          timerPromise = $timeout(function() {
            if (queue.length)
            {
              showOverlay();
            }
          }, scope.wcOverlayDelay ? scope.wcOverlayDelay : 500);
        }
      }

      function processResponse() {
        queue.pop();
        if (queue.length === 0) {
          $timeout(function() {
            if (queue.length === 0)
            {
              hideOverlay();
            }
          }, scope.wcOverlayDelay ? scope.wcOverlayDelay : 500);
        }
      }

    };

    var directive = {
      restrict: 'EA',
      transclude: true,
      scope: {
        'nibOverlayDelay': '@'
      },
      templateUrl: 'modules/directives/nib-overlay/overlay.html',
      require: 'ngModel',
      link : link
    };

    return directive;
  }

})();
