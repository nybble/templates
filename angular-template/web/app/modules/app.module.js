(function() {
  'use strict';

  angular.module('myApp', [
    'ui.router',
    'app.dashboard',
    'app.directives'
  ]);

})();
